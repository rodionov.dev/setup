#!/bin/bash

read -p "Project domain: " DOMAIN
read -p "Pma domain: " PMA_DOMAIN
read -p "User password: " USER_PASSWORD
read -p "Db root password: " DB_ROOT_PASSWORD
read -p "Git: " GIT

echo "Project domain: $DOMAIN
Pma domain: $PMA_DOMAIN
User password: $USER_PASSWORD
DB root password: $DB_ROOT_PASSWORD
Git: $GIT"

read -p "Do you want to proceed? (y/n) " yn
while true; do
        case $yn in
        [Yy]* ) echo ok, we will proceed; break;;
        [Nn]* ) echo exiting...;
                exit;;
        * ) echo invalid response;
        esac
done

useradd -m -p $USER_PASSWORD -s /bin/bash user
usermod -aG sudo user
sudo add-apt-repository ppa:ondrej/php
apt install nginx mysql-server php8.2 php8.2-fpm php8.2-bcmath php8.2-fpm php8.2-xml php8.2-mysql php8.2-zip php8.2-intl php8.2-ldap php8.2-gd php8.2-cli php8.2-bz2 php8.2-curl php8.2-mbstring php8.2-pgsql php8.2-opcache php8.2-soap php8.2-cgi certbot python3-certbot-nginx supervisor composer --yes
sudo mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password by '$DB_ROOT_PASSWORD';"

cmd="ssh-keygen -t rsa -N '' -f ~/.ssh/id_rsa <<< y"
sudo -H -u user bash -c "$cmd"

#копирование ключей для гита
cmd="cat ~/.ssh/id_rsa.pub"
sudo -H -u user bash -c "$cmd"
read -p "Copy id_rsa.pub to gitlab access token and press any key to continue... " s

cmd="cat ~/.ssh/id_rsa"
sudo -H -u user bash -c "$cmd"
read -p "Copy id_rsa to gitlab SSH_PRIVATE_KEY var and press any key to continue... " s

cmd="mkdir /home/user/www && cd /home/user/www && cp ~/.ssh/id_rsa.pub ~/.ssh/authorized_keys && chmod 700 ~/.ssh/authorized_keys && git config --global user.name 'Server' && git clone $GIT $DOMAIN && cd $DOMAIN && cp .env.example .env && composer install"
sudo -H -u user bash -c "$cmd"

#Работа с файлом domain.conf
sed -e "s/DOMAIN/$DOMAIN/g" domain.conf >> "$DOMAIN.conf"
mv $DOMAIN.conf /etc/nginx/sites-available
ln -s /etc/nginx/sites-available/$DOMAIN.conf /etc/nginx/sites-enabled/
#Работа с файлом pma_domain.conf
sed -e "s/PMA_DOMAIN/$PMA_DOMAIN/g" pma_domain.conf >> "$PMA_DOMAIN.conf"
mv $PMA_DOMAIN.conf /etc/nginx/sites-available
ln -s /etc/nginx/sites-available/$PMA_DOMAIN.conf /etc/nginx/sites-enabled/

usermod -a -G user www-data

apt install phpmyadmin --yes

sudo usermod -a -G user www-data
sudo update-alternatives --set php /usr/bin/php8.2
chown www-data:www-data -R /usr/share/phpmyadmin
chown user:www-data -R /home/user/www
certbot --nginx -d $DOMAIN -d $PMA_DOMAIN --non-interactive --agree-tos -m webmaster@makeroi.ru
systemctl restart nginx php8.3-fpm
